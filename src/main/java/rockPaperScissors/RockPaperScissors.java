package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    String playerSelection;
    String computerSelection;
    String winnerMessage;
    String continueInput;
    boolean correctInput = false;
    boolean quit = false;
    int randomNum;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        while(quit == false){
            System.out.println("Let's play round " + roundCounter);
            
            while(correctInput == false){
                playerSelection = readInput("Your choice (Rock/Paper/Scissors)?");
                if(playerSelection.equalsIgnoreCase("q") || playerSelection.equalsIgnoreCase("quit")){
                    quit = true;
                    break;
                } else if(playerSelection.equalsIgnoreCase("r") || playerSelection.equalsIgnoreCase("rock"))
                    playerSelection = "rock";
                else if(playerSelection.equalsIgnoreCase("s") || playerSelection.equalsIgnoreCase("scissors"))
                    playerSelection = "scissors";
                else if(playerSelection.equalsIgnoreCase("p") || playerSelection.equalsIgnoreCase("paper"))
                    playerSelection = "paper";
                else {
                    System.out.println("I do not understand cardboard. Could you try again?");
                    continue;
                }
                break;
            }
            
            if(quit == true)
                break;
            
            randomNum = ThreadLocalRandom.current().nextInt(0, 3);
            computerSelection = rpsChoices.get(randomNum);
            
            if(playerSelection == computerSelection){
                winnerMessage = "It's a tie!";
            } else if((playerSelection == "rock" && computerSelection == "scissors") || (playerSelection == "scissors" && computerSelection == "paper") || (playerSelection == "paper" && computerSelection == "rock")){
                winnerMessage = "Human wins!";
                humanScore++;
            } else{
                winnerMessage = "Computer wins!";
                computerScore++;
            }
            
            System.out.println("Human chose " + playerSelection + ", computer chose " + computerSelection + ". " + winnerMessage);
            System.out.println("Score: human " + humanScore + ", computer " + computerScore);
            
            while (correctInput == false){
                continueInput = readInput("Do you wish to continue playing? (y/n)?");
                if(continueInput.equals("y")){
                    break;
                } else if(continueInput.equals("n")){
                    quit = true;
                } else {
                    System.out.println(continueInput + " is not a valid input, try again!");
                    continue;
                }
                break;
            }
            roundCounter++;
            
        }
        System.out.println("Bye bye :)");
    }
    
    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
